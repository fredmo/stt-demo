let current = ''

function setFile (path) {
    // const path = img.getAttribute("data-path")

    if (current == path) return

    const oldAudioPlayer = document.getElementById('audio-player')

    if (!!oldAudioPlayer) {
        oldAudioPlayer.remove()
    }

    const newAudioPlayer = document.createElement('audio')
    const att = document.createAttribute('controls')
    att.value = true
    newAudioPlayer.setAttributeNode(att)
    newAudioPlayer.id = 'audio-player'

    const newSource = document.createElement('source')
    newSource.src = 'assets/' + path

    newAudioPlayer.appendChild(newSource)

    const audioPlayerContainer = document.getElementById('audio-player-container')
    audioPlayerContainer.appendChild(newAudioPlayer)

    current = path
    
    //get data result
    if (typeof DataExtractor !== 'undefined') {
        DataExtractor.showResult(current)
    }
}

const audioFile = [
    '20190827_1638_83759_83774.Elena.wav',
    '20190827_1653_83759_83769.winnie.wav'
]
window.onload = () => {
    const selectionRegion = document.getElementById('file-selection-region-content')
    for (file of audioFile) {
        console.log(file)
        const fileContainer = document.createElement('div')
        fileContainer.className = 'file'
        fileContainer.setAttribute('path', file)
        fileContainer.onclick = function () {
            setFile(fileContainer.getAttribute('path'))
        }

        const icon = document.createElement('i')
        icon.className = 'material-icons'
        icon.innerHTML = 'audiotrack'
        
        const header = document.createElement('h')
        header.innerHTML = file

        fileContainer.appendChild(icon)
        fileContainer.appendChild(header)

        selectionRegion.appendChild(fileContainer)
    }
}