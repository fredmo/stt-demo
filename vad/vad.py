from pyannote.database import get_protocol
from pyannote.database import FileFinder
from pyannote.audio.labeling.extraction import SequenceLabeling
from pyannote.audio.signal import Peak

prefix_path = './vad/pyannote-audio/'

class Vad:
    def __init__(self, path):
        print('target', path)
        self.target = {
            'uri': path.split('/')[-1],
            'audio': '.{}'.format(path)
        }
        pass

    def detect_speaker_changes(self):
        # pretrained models
        # speech activity detection model trained on AMI training set
        SAD_MODEL = (prefix_path + 'tutorials/models/speech_activity_detection/train/'
                    'AMI.SpeakerDiarization.MixHeadset.train/weights/0280.pt')

        # speaker change detection model trained on AMI training set
        SCD_MODEL = (prefix_path + 'tutorials/models/speaker_change_detection/train/'
                    'AMI.SpeakerDiarization.MixHeadset.train/weights/0870.pt')

        # speaker embedding model trained on VoxCeleb1
        EMB_MODEL = (prefix_path + 'tutorials/models/speaker_embedding/train/'               
                    'VoxCeleb.SpeakerVerification.VoxCeleb1.train/weights/2000.pt')

        print('testing SAD_MODEL', self.target)

        preprocessors = {'audio': FileFinder()}
        protocol = get_protocol('AMI.SpeakerDiarization.MixHeadset', preprocessors=preprocessors)
                                
        # initialize SAD & SCD sequence labeling models
        sad = SequenceLabeling(model=SAD_MODEL)
        scd = SequenceLabeling(model=SCD_MODEL)

        sad_scores = sad(self.target)

        from pyannote.audio.signal import Binarize
        binarize = Binarize(offset=0.94, onset=0.70, log_scale=True)
        speech = binarize.apply(sad_scores, dimension=1)

        print('sad_scores', sad_scores)

        # iterate over speech segments (as `pyannote.core.Segment` instances)
        # for segment in speech:
        #     print(segment.start, segment.end)

        print('length', len(speech))
        # scd_scores = scd(self.target)
        # print('scd_scores', scd_scores)

        # peak = Peak(alpha=0.08, min_duration=0.40, log_scale=True)
        # partition = peak.apply(scd_scores, dimension=1)
        # for idx, segment in enumerate(partition):
        #     yield idx, max(0, int(segment.start*1000)), int(segment.end*1000)