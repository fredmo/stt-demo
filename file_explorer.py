import os
import glob
from pathlib import Path

root_path = './static/audio'

class FileExplorer:

    def __init__(self):
        pass

    def browse(self):
        return self.getWavFiles(root_path)

    def getWavFiles(self, path):
        res = []
        allfiles = os.listdir(path)
        print('allfiles', allfiles)
        for file in allfiles:
            if file.endswith('.wav'):
                res.append({
                    'path': path,
                    'file_name': 'audio/' + file
                })

        return res

