let current = ''

async function setFile (img) {
    const path = img.getAttribute("data-path")

    if (current == path) return

    const oldAudioPlayer = document.getElementById('audio-player')

    if (!!oldAudioPlayer) {
        oldAudioPlayer.remove()
    }

    const newAudioPlayer = document.createElement('audio')
    const att = document.createAttribute('controls')
    att.value = true
    newAudioPlayer.setAttributeNode(att)
    newAudioPlayer.id = 'audio-player'

    const newSource = document.createElement('source')
    newSource.src = path

    newAudioPlayer.appendChild(newSource)

    const audioPlayerContainer = document.getElementById('audio-player-container')
    audioPlayerContainer.appendChild(newAudioPlayer)

    current = path
    
    //get data result
    if (typeof DataExtractor !== 'undefined') {
        await DataExtractor.showResult(current)
    }
}